FROM node:6.3.0

RUN mkdir -p /app
COPY ./app/ /app

RUN cat /app/package.json

WORKDIR /app

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]

