NodeJS / Express  application
===================

Hey! this is our first application in our workshop, this one has 1  container that we are going to be talking to get data.

The application has 2 main endpoints:

http://host:3000/todo <-- that returns a list of all the items on the system
http://host:3000/todo/:user_id <-- returns a list of todo lists by user_id (which will be available later)

Where host is the IP address assigned to your container.

In this moment you can access the application from port 5000 which is a php app running over a apache webserver.

In order to setup the application you have to run this commands: 

docker commands
-------------

```
# Create the virtual machine
docker-machine create --driver virtualbox ws-node

# point our environment to our vm
eval $(docker-machine env ws-node) 

# build the docker image
docker-compose build 

# initiate our service
docker-compose up 
```

During the session, we are going to review the project: how it was build and how to create your own container.
