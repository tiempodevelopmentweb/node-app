var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// Load the routes
var routes = require('./routes/index');
var todos = require('./routes/todo');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', routes);
app.use('/todos', todos);


app.listen(3000, function () {
  console.log('express app listening on port 3000!');
});
