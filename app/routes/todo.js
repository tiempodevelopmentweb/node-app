var express = require('express');
var router = express.Router();

var max = 1000000,
    min = 4;

var todos = [
    {
        id: 1,
        name: 'My new todo',
        user_id: 1,
        items:[
            {
                id: 1,
                description: 'create the workshop',
            },
            {
                id: 2,
                description: 'create the slides for day 1.',
            },
            {
                id: 3,
                description: 'upload code to bitbucket',
            },
        ]
    },

    {
        id: 2,
        name: 'My new todo list?',
        user_id: 2,
        items:[
            {
                id: 11,
                description: 'define something',
            },
            {
                id: 23,
                description: 'do something else.',
            },
            {
                id: 33,
                description: 'review something',
            },
        ]
    },

    {
        id: 3,
        name: 'daft punk song',
        user_id: 2,
        items:[
            {
                id: 91,
                description: 'work it harder',
            },
            {
                id: 4,
                description: 'make it better',
            },
            {
                id: 5,
                description: 'do it faster',
            },
        ]
    }

];


/* GET TODOs listing. */
router.get('/', function(req, res) {
    res.send(todos);
});

/* GET TODOs listing. */
router.get('/:user_id', function(req, res) {
    var returnData = [];

    var user_id = parseInt(req.params.user_id);

    if(user_id) {
        returnData = todos.filter( function(element, index) {
            return (element.user_id === user_id);
        });
    }

    res.send(returnData);
});


function randomId(){
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
/* creates a new element in our list */
router.post('/', function(req, res) {
    var newId =  randomId()
    var todoItems = []
    if(req.body.items) {
        todoItems = req.body.items.map(function(item, index) {
            return {
                id: randomId(),
                description: item.description
            };
        });
    }

    var newTodo = {
        id: newId,
        name: req.body.name,
        user_id: req.body.user_id,
        items: todoItems
    };

    todos.push(newTodo)
    res.send(newTodo);
});


module.exports = router;
